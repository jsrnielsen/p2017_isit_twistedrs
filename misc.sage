r"""
Helper functions
"""

import itertools
import sys, hashlib, inspect

load("brouwer_zimmermann.sage")

class SolutionFound(Exception):
    """Use exceptions for exiting a deep search algorithm"""
    def __init__(self, solution):
        self.solution = solution


### Saving and Loading

data_folder = os.getcwd() + "/data"

def identifier_to_path(identifier):
    return data_folder +"/"+ identifier + ".sobj"

if not os.path.isdir(data_folder):
    raise Exception("The \"data\" sub-folder does not exist. Are you sure you launched Sage in the right directory? If so, please create the subfolder 'data'.")

def _permanent_cache_identifier_old(f, args, kwargs):
    def cleanup_arg(a):
        if isinstance(a, int):
            return Integer(a)
        elif isinstance(a, float):
            return RR(a)
        else:
            return a
    args = [ cleanup_arg(a) for a in args ]
    # Remove from kwargs arguments that take the default value
    spec = inspect.getargspec(f)
    for var, defval in zip(spec.args[-len(spec.defaults):], spec.defaults):
        try:
            if kwargs[var] == defval:
                del kwargs[var]
            else:
                kwargs[var] = cleanup_arg(kwargs[var])
        except KeyError:
            pass
    return f.__name__ + hashlib.sha1(dumps(args) + "- & -" + dumps(kwargs)).hexdigest()

def _permanent_cache_identifier(f, args, kwargs):
    def cleanup_arg(a):
        if isinstance(a, int):
            return Integer(a)
        elif isinstance(a, float):
            return RR(a)
        elif a in Rings:
            raise ValueError("Algebraic structures have bad hash behaviour and are unsupported for permanent caching.")
        else:
            return a
    args = tuple( cleanup_arg(a) for a in args )
    # Remove from kwargs arguments that take the default value
    spec = inspect.getargspec(f)
    for var, defval in zip(spec.args[-len(spec.defaults):], spec.defaults):
        try:
            if kwargs[var] == defval:
                del kwargs[var]
            else:
                kwargs[var] = cleanup_arg(kwargs[var])
        except KeyError:
            pass
    return f.__name__ + "_" + str(hash(args)) + "_" + str(hash(frozenset(kwargs.itervalues())))

def _has_permanent_cache(f, args, kwargs):
    return os.path.isfile(identifier_to_path(_permanent_cache_identifier(f, args, kwargs)))

def _del_permanent_cache(f, args, kwargs):
    try:
        os.remove(identifier_to_path(_permanent_cache_identifier(f, args, kwargs)))
        return True
    except OSError:
        return False

# A general persistent cache decorator, that takes a load function and a save function
def permanently_cached_function_mod(load_fun, save_fun):
    r"""
    Decorator for functions to automatically cache their output in files.

    INPUT:

    - ``load_fun`` -- A function that takes a path and returns a Python object

    - ``save_fun`` -- A function that takes a Python object and a file path, and
                  writes the object to that file in a format that ``load_fun`` can retrieve.

    """
    def _permanently_cached_function(f):
        @sage_wraps(f)
        def new_f(*args, **kwargs):
            if "replace_cache" in kwargs:
                del kwargs["replace_cache"]
                _del_permanent_cache(f, args, kwargs)
            identifier = _permanent_cache_identifier(f, args, kwargs)
            if os.path.isfile(identifier_to_path(identifier)):
                try:
                    return load_fun(identifier_to_path(identifier))
                except Exception, err: #cache corrupt?
                    print "\tPermanent cache was corrupted. Error was:\n\t%s: %s" % (type(err), err.message)
            res = f(*args, **kwargs)
            save_fun(res, identifier_to_path(identifier))
            return res
        def get_identifier(*args, **kwargs):
            return _permanent_cache_identifier(f, args, kwargs)
        new_f.get_identifier = get_identifier
        def has_cache(*args, **kwargs):
            return _has_permanent_cache(f, args, kwargs)
        new_f.has_cache = has_cache
        def del_cache(*args, **kwargs):
            return _del_permanent_cache(f, args, kwargs)
        new_f.delete_cache = del_cache
        new_f.f = f
        return new_f
    return _permanently_cached_function

# Instantiate the common use decorator
permanently_cached_function = permanently_cached_function_mod(load, save)


### Saving and Loading with Yaml 
import yaml

def yaml_load(path):
    stream = file(path, 'r')
    return yaml.load(stream)

def yaml_save(obj, path):
    stream = file(path, 'w')
    yaml.dump(obj, stream)

permanently_cached_function_yaml = permanently_cached_function_mod(yaml_load, yaml_save)

def yaml_of_field_elem(a):
    if a == infinity:
        return -101
    else:
        return int(a.rank())

def yaml_to_field_elem(i, F):
    if i == -101:
        return infinity
    else:
        return F.unrank(i)


def linear_code_to_yaml(dumper, C):
    d = {
        'q': int(C.base_field().cardinality()),
        'n': int(C.length()),
        'k': int(C.dimension()),
        'G': [ [ yaml_of_field_elem(a) for a in row ] for row in C.generator_matrix().rows() ]
        }
    return dumper.represent_mapping(u'!linearCode', d)

def linear_code_from_yaml(loader, node):
    d = loader.construct_mapping(node, deep=True)
    F = GF(Integer(d['q']))
    n = Integer(d['n'])
    k = Integer(d['k'])
    G_l = d['G']
    G = matrix(F, [ [ yaml_to_field_elem(i, F) for i in row ] for row in G_l ] )
    return LinearCode(G)

_C = LinearCode(identity_matrix(GF(2),2)) # prime fields
yaml.add_representer(_C.__class__, linear_code_to_yaml)
_C = LinearCode(identity_matrix(GF(2^2),2)) # extension fields
yaml.add_representer(_C.__class__, linear_code_to_yaml)
yaml.add_constructor(u'!linearCode', linear_code_from_yaml)

### Data Analysis

def each_non_grs_possible_parameters(rangeq, n_fun=None, k_fun=None):
    if rangeq in ZZ:
        for (q,n,k) in each_non_grs_possible_parameters([rangeq]):
            yield (q,n,k)
    else:
        if n_fun is None:
            n_fun = lambda q: range(4, q+2)
        if k_fun is None:
            k_fun = lambda q,n: range(3, n-2)
        for q in rangeq:
            if Integer(q).is_prime_power():
                for n in n_fun(q):
                    for k in k_fun(q,n):
                        yield (q, n, k)

def stats_inequivalence_map(d):
    r"""
    Return four key statistics about the given canonical codes map::

    - ``total`` -- the total number of codes in the map.

    - ``unique`` -- the number of inequivalent codes in the map.

    - ``non_grs`` -- the number of unique classes which are non-GRS.

    - ``total_grs`` -- the total number of codes which are GRS.
    """
    total = sum(len(codes) for codes in d.values())
    unique = len(d)
    non_grs = 0
    total_grs = 0
    for (canon, codes) in d.iteritems():
        if not is_GRS(canon):
            non_grs += 1
        else:
            total_grs += len(codes)
    return (total, unique, non_grs, total_grs)

def print_stats_inequivalence_map(d, name="codes"):
    r"""
    Print the results of ``stats_inequivalence_map``.
    """
    (total, unique, non_grs, total_grs) = stats_inequivalence_map(d)
    print "\tTotal number of %s: %s" % (name, total)
    print "\tUnique MDS-codes from %s: %s" % (name, unique)
    print "\tUnique non-GRS codes: %s" % non_grs
    print "\tUnique GRS codes: %s.\t Total number of GRS codes: %s" % (unique - non_grs, total_grs)
    print

def summary_table_stats_inequivalence_map(q, codemap_fun):
    r"""
    Make a table summarising the code count statistics over a field size.
    """
    def data_fun(n, k):
        if k <= 2 or k >= n-2:
            return None
        else:
            (total, unique, non_grs, grs_twists) = stats_inequivalence_map(codemap_fun(q, n, k))
            if total == 0:
                return None
            else:
                return "%s/%s/%s" % (total, unique, non_grs)
    return summary_table_over_field_size(q, data_fun)

def summarise_single_twisted_code_set(S):
    r"""
    Print some statistics for a set of single twisted codes
    """
    print "%s codes" % len(S)
    try:
        hooks = set( C.hook() for C in S )
        etas  = set( C.twist_coefficient() for C in S )
        twists = set( C.twist() for C in S )
        F = iter(S).next().base_ring()
        not_evals = set(F.list())
        for C in S:
            not_evals = not_evals.difference(C.evaluation_points())
        print "Hooks: %s\nEtas: %s\nTwists: %s\nNot evaluation points: %s" % (hooks, etas, twists, not_evals)
    except AttributeError:
        pass

def summarise_inequivalence_map(d, detailed=True):
    r"""
    Print a summary of the elements in the dictionary ``d: canonical repr. ->
    list of codes``.
    """
    if not d:
        print "Empty map"
        return
    reprs = sorted([ len(codes) for codes in d.itervalues() ])
    print ("%s canonical representatives with %s codes in total" % (len(d), sum(reprs)))
    print ("Distribution: Min: %s, Median: %s. Max: %s" % (reprs[0], reprs[len(reprs)//2], reprs[-1]))
    sizes = dict()
    for s in reprs:
        if s in sizes:
            sizes[s] += 1
        else:
            sizes[s] = 1
    print "Size frequencies: %s" % sizes

    if detailed:
        for Cc in d.keys():
            summarise_single_twisted_code_set(d[Cc])
            print "Is MDS: %s" % (is_MDS(Cc))
            print "Is GRS: %s" % (is_GRS(Cc))
            print

def summarise_difference_inequivalence_maps(d1, d2, name1="d1", name2="d2"):
    (s1, s2, both) = difference_inequivalence_maps(d1, d2)
    def count_repr(s, d):
        return sum(len(d[C]) for C in s)
    print "%s canonical classes in %s and not in %s, representing %s codes." % (len(s1), name1, name2, count_repr(s1, d1))
    print "%s canonical classes in %s and not in %s, representing %s codes." % (len(s2), name2, name1, count_repr(s2, d2))
    print "%s canonical classes in both maps, representing %s codes in %s and %s codes in %s." % (len(both), count_repr(both, d1), name1, count_repr(both, d2), name2)

def summary_table_from_dict(d, title="", frame = False, align = 'left'):
    r"""
    Make a table given a dict d: `(i,j) -> data`.
    """
    li,lj = zip(*list(d.iterkeys()))
    li = sorted(set(li))
    lj = sorted(set(lj))
    rows = []
    rows.append([ title ] + lj)
    for i in li:
        row = [ i ]
        for j in lj:
            row.append(d[(i,j)])
        rows.append(row)
    return table(rows, header_row=True, header_column=True, frame=frame, align=align)


def summary_table_over_field_size(q, data_fun, frame = False, align = 'left'):
    r"""
    Create a table summarising some data across all lengths and dimensions for
    some fixed field size.

    The table takes a function ``data_fun: (n,k) -> data``. Data is anything
    printable. ``data_fun`` should return ``None`` if the input pair does not
    apply.

    The table will be truncated to the range of `n` and `k` which contains
    non-None, non-zero values.
    """
    data = { (n,k): data_fun(n, k) for k in range(1, q+2) for n in range(1, q+2) }
    if all( value == None or value == 0 for value in data.values() ):
        return "Empty table"
    ns, ks = zip( *( params for params in data.iterkeys() if data[params] != None and data[params] != 0 ) )
    min_k, max_k = min(ks), max(ks)
    min_n, max_n = min(ns), max(ns)

    rows = []
    rows.append(["n \ k"] + list(range(min_k, max_k+1)))
    for n in range(min_n, max_n+1):
        row = [ n ]
        for k in range(min_k, max_k+1):
            row.append(data[(n,k)])
        rows.append(row)

    return sage.misc.table.table(rows, header_row=True, header_column=True, frame=frame, align=align)

def all_are_MDS(codes):
    r"""
    Verify that all the codes are MDS. ``codes`` can be a set of codes or an
    inequivalence map.
    """
    for C in codes:
        if not is_MDS(C):
            return False
    return True

### T-sum sets

def t_sum_avoids(S, t, delta):
    r"""
    Return whether any t-subset of S sums up to something different from delta.
    """
    return all( sum(A) != delta for A in itertools.combinations(S, t) )

def t_sum_avoidance_set(S, t):
    r"""
    Return the set of elements that is t-sum avoided by S.
    """
    F = iter(S).next().parent()
    U = { e: 1 for e in F }
    rem = F.cardinality()
    for A in itertools.combinations(S, t):
        s = sum(A)
        if U[s]:
            rem -= 1
            U[s] = 0
            if rem == 0:
                return []
    return set( e for e in F if U[e] == 1 )

### Other

def minimum_weight_codeword(C):
    d = brouwer_zimmermann(C)
    for c in C:
        if c.hamming_weight() == d:
            return c

def common_order(xs):
    """Return the lcm of the orders of the elements"""
    return lcm([x.multiplicative_order() for x in xs if not x.is_zero()])

def filter_GRS_codes(code_set):
    r"""
    Given a set of codes, remove those which are GRS codes
    """
    if instanceof(code_set, dict):
        return { c: filter_GRS_codes(codes) for (c,codes) in code_set.iteritems() }
    return set( C for C in code_set if not is_GRS(C) )

def inequivalence_map(code_set, equivalence="linear", progress=False):
    r"""
    From a set of codes, create a dictionary ``canonical repr -> code list``.
    """
    d = dict()
    systematics = dict()
    N = len(code_set)
    if progress:
        print_progress(0, N)
        i = 0
    for C in code_set:
        S = C.systematic_generator_matrix()
        if S in systematics:
            canon = systematics[S]
        else:
            (canon,_) = C.canonical_representative(equivalence)
            S.set_immutable()
            systematics[S] = canon
        if canon in d:
            d[canon].append(C)
        else:
            d[canon] = [ C ]
        if progress:
            print_progress(i, N)
            i += 1
    return d

def merge_inequivalence_maps(d1, d2):
    r"""
    From a maps as produced by `inequivalence_map`, return a common map with all
    the codes in.

    NOTE::

        The canonical codes in `d1` and `d2` (the keys) is assumed to possibly
        overlap, but the codes mapped to (the values) are supposed to be
        disjoint.
    """
    dm = copy(d1)
    for (C, Cs) in d2.iteritems():
        if C in dm:
            dm[C].extend(Cs)
        else:
            dm[C] = Cs
    return dm

def difference_inequivalence_maps(d1, d2):
    r"""
    Return three sets of canonical codes from the maps ``d1`` and ``d2``: those
    mapped to in ``d1`` but not in ``d2``, those from ``d2`` not in ``d1``, and
    those in both.
    """
    s1, s2, both = set(), set(), set()
    for C in d1:
        if C in d2:
            both.add(C)
        else:
            s1.add(C)
    for C in d2:
        if not C in d1:
            s2.add(C)
    return (s1, s2, both)


### MDS and GRS checks

def _mds_check_all_nonzero(A):
    r"""MDS Check: All elements of A are non-zero"""
    for a in A:
        for e in a:
            if e.is_zero():
                return False
    return True

def _mds_check_two_minors_nonzero(A):
    r"""MDS Check: All 2x2 minors of A are non-zero"""
    for m in A.minors(2):
        if m.is_zero():
            return False
    return True

def _grs_check_three_minors_zero(A):
    r"""GRS Check: All 3x3 minors of the component-wise inverson of A are zero"""
    Amin = matrix(A.base_ring(), A.nrows(), A.ncols(), lambda i,j: 1/A[i,j])
    for m in Amin.minors(3):
        if not m.is_zero():
            return False
    return True

def is_maybe_MDS(C):
    r"""A cheap check which can disprove MDS (but not prove it).

    In other words, if ``is_maybe_MDS`` returns ``True`` then `C` might be MDS.
    But if it returns ``False``, then it is definitely not MDS.
    """
    Gsys = C.systematic_generator_matrix()
    k = C.dimension()
    I = Gsys.transpose()[:k]
    A = Gsys.transpose()[k:]
    return I.is_one() and _mds_check_all_nonzero(A) and _mds_check_all_nonzero(A)

def is_MDS(C):
    r"""
    Return if `C` is MDS.
    """
    return is_maybe_MDS(C) and (brouwer_zimmermann(C) == C.length() - C.dimension() + 1)


def is_GRS(C, details=False):
    r"""Returns whether the linear code `C` is a GRS code.

    If details is True, it returns a "reason code":

    - 1   --  The code IS GRS
    - -1  --  The code does not have a systematic matrix with identity at beginning (not MDS).
    - -2  --  If [I|A] is the systematic matrix, then A has zero-elements (not MDS).
    - -3  --  A has 2x2 minors which are zero (not MDS).
    - -4  --  A has 3x3 minors which are non-zero (not GRS).

    """
    Gsys = C.systematic_generator_matrix()
    k = C.dimension()
    I = Gsys.transpose()[:k]
    A = Gsys.transpose()[k:]
    def _is_GRS():
        if not I.is_one():
            return -1
        if not _mds_check_all_nonzero(A):
            return -2
        if not _mds_check_two_minors_nonzero(A):
            return -3
        if not _grs_check_three_minors_zero(A):
            return -4
        return 1
    if details:
        return _is_GRS()
    else:
        return _is_GRS() > 0

def complete_MDS(C):
    r"""
    Return a column which can be adjoined to the generator matrix of C such that
    C is still MDS, or return None if no such exist.

    TODO: Current implementation is extremely inefficient.
    """
    if not is_MDS(C):
        return None
    F = C.base_field()
    G = C.systematic_generator_matrix()
    k = C.dimension()
    for v in ProjectiveSpace(F, k-1):
        vl = list(v)
        if not F.zero() in vl:
            A = G.augment(matrix(F, k, 1, vl))
            if is_MDS(LinearCode(A)):
                return v
    return None




### OTHER STUFF


# From codinglib/util
print_progress_counter=1000
print_progress_width=80
def print_progress(cur_iter, total_iters, width=None):
    """Prints a progress bar for long-running computations. Call each iteration
    of the computation, giving current iteration and total number of
    iterations."""
    global print_progress_counter
    global print_progress_width
    if total_iters == 0:
        return
    completed = 1. * print_progress_width * cur_iter/total_iters
    # print completed , print_progress_counter
    if print_progress_counter > completed:
        # probably a restart
        print_progress_width = width
        if print_progress_width is None:
            import os
            _, columns = os.popen('stty size', 'r').read().split()
            print_progress_width = int(columns) - 3
        print("|"+"-"*print_progress_width+"|")
        sys.stdout.write("|")
        sys.stdout.flush()
        print_progress_counter = 0
    while print_progress_counter < completed - 1:
        print_progress_counter += 1
        sys.stdout.write(".")
        sys.stdout.flush()
    if cur_iter >= total_iters-1:
        sys.stdout.write(".")
        print("|")
        
