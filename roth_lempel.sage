r"""
Implementation of Roth-Lempel's MDS codes.
"""

from sage.coding.linear_code import AbstractLinearCode, LinearCodeSyndromeDecoder, LinearCodeGeneratorMatrixEncoder
from sage.coding.encoder import Encoder
from sage.coding.codes_catalog import *

class RothLempelCode(AbstractLinearCode):
    r"""
    Roth-Lempel code.

    INPUT:

    - evaluation_points -- A list of evaluation points. Use ``infinity`` to add the point at infinity.

    - dimension -- The wanted dimension.

    - delta -- The base for the twist evaluation.
    """

    _registered_encoders = {}
    _registered_decoders = {}

    def __init__(self, evaluation_points, dimension, delta, check = True):
        length = len(evaluation_points) + 1

        if dimension not in ZZ or dimension > length or dimension < 1:
            raise ValueError("The dimension must be a positive integer at most the length of the code.")
        self._dimension = dimension

        # Find the common parent field of everything
        self._affine_points = vector([ a for a in evaluation_points if a != infinity ])
        F = (delta * self._affine_points[0]).parent()
        if F.is_finite() == False or F.is_field() == False:
            raise ValueError("Base field must be a finite field (and %s is not one)" % F)

        self._evaluation_points = [ F(a) if a != infinity else infinity for a in evaluation_points ]
        self._delta = F(delta)

        if check:
            assert t_sum_avoids(self._affine_points, dimension-1, delta) ,\
              "The given delta does not avoid %s-sums of the evaluation points" % (dimension-1)


        super(RothLempelCode, self).__init__(F, length, "Vector", "Syndrome")

    def _repr_(self):
        return "[%s, %s] Roth-Lempel code over GF(%s)"\
                % (self.length(), self.dimension(), self.base_field().cardinality())

    def _latex_(self):
        return "[%s, %s] L-\\textnormal{Twisted GRS code over } %s"\
                % (self.length(), self.dimension(), self.base_field()._latex_())

    def is_projective(self):
        return infinity in self.evaluation_points()

    def twist(self):
        """
        Return the twist.
        """
        return self._delta

    def affine_points(self):
        return self._affine_points

    def evaluation_points(self):
        return self._evaluation_points

class EncoderRothLempel(Encoder):

    def __init__(self, code):
        super(EncoderRothLempel, self).__init__(code)

    def _repr_(self):
        r"""
        Returns a string representation of ``self``.
        """
        return "Encoder for %s" % self.code()
    
    @cached_method
    def generator_matrix(self):
        C = self.code()
        F = C.base_ring()
        dimension = C.dimension()
        evals = C.evaluation_points()

        def vanCell(i,j):
            if evals[j] != infinity:
                return evals[j]^i
            else:
                return 1 if i == dimension-1 else 0
        Van = matrix(F, dimension, len(evals), vanCell)
        A = matrix(F, 1, dimension, [ 0 ] * (dimension-2) + [1, C._delta]).transpose()
        G = Van.augment(A)
        return G

RothLempelCode._registered_encoders["Vector"] = EncoderRothLempel

### Yaml dumping
import yaml

def roth_lempel_to_yaml(dumper, C):
    d = {
        'q': int(C.base_field().cardinality()),
        'evals': [ yaml_of_field_elem(a) for a in C.evaluation_points() ],
        'k': int(C.dimension()),
        'delta': yaml_of_field_elem(C.twist()),
        }
    return dumper.represent_mapping(u'!rothLempel', d)

def roth_lempel_from_yaml(loader, node):
    d = loader.construct_mapping(node, deep=True)
    F = GF(Integer(d['q']))
    k = Integer(d['k'])
    delta = yaml_to_field_elem(d['delta'], F)
    evals = [ yaml_to_field_elem(i, F) for i in d['evals'] ]
    return RothLempelCode(evals, k, delta)

# Because of the category framework, we need to create a dummy element to get
# the correct class to register
_C = RothLempelCode(GF(2).list(), 1, 1, check=False)
yaml.add_representer(_C.__class__, roth_lempel_to_yaml)
_C = RothLempelCode(GF(4,'a').list(), 1, GF(4,'a').one(), check=False) #extension fields
yaml.add_representer(_C.__class__, roth_lempel_to_yaml)
yaml.add_constructor(u'!rothLempel', roth_lempel_from_yaml)

