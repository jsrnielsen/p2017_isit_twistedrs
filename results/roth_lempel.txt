 ---- SUMMARY TABLE COUNTING ROTH-LEMPEL CODES ---- 
Summary table for GF(7):  Total / Inequivalent / Non-GRS
  n \ k | 3
+-------+--------+
  6     | 56/2/2

Summary table for GF(8):  Total / Inequivalent / Non-GRS
  n \ k | 3         4        5
+-------+---------+--------+--------+
  6     | 238/4/4   None     None
  7     | 84/1/1    56/1/1   None
  8     | 36/1/1    None     28/1/1

Summary table for GF(9):  Total / Inequivalent / Non-GRS
  n \ k | 3         4
+-------+---------+---------+
  6     | 576/4/4   None
  7     | 144/2/2   168/3/3
  8     | None      24/1/1

Summary table for GF(11):  Total / Inequivalent / Non-GRS
  n \ k | 3            4            5
+-------+------------+------------+---------+
  6     | 2992/10/10   None         None
  7     | 1584/10/10   1309/10/10   None
  8     | 352/4/4      77/2/2       352/4/4

Summary table for GF(13):  Total / Inequivalent / Non-GRS
  n \ k | 3             4            5            6
+-------+-------------+------------+------------+---------+
  6     | 10816/19/19   None         None         None
  7     | 8944/35/35    7020/36/36   None         None
  8     | 4160/16/16    1404/10/10   3328/16/16   None
  9     | 832/5/5       None         None         832/5/5



 ---- GROUP TWISTED VS ROTH-LEMPEL ---- 
 
(q, n, k): (7, 6, 3)
2 canonical representatives with 56 codes in total
Distribution: Min: 14, Median: 42. Max: 42
Size frequencies: {42: 1, 14: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (8, 6, 3)
4 canonical representatives with 238 codes in total
Distribution: Min: 14, Median: 14. Max: 196
Size frequencies: {196: 1, 14: 3}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
3 canonical classes in R-L and not in (+)-tsum-TRS, representing 42 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 196 codes in R-L and 49 codes in (+)-tsum-TRS.

(q, n, k): (8, 7, 3)
1 canonical representatives with 84 codes in total
Distribution: Min: 84, Median: 84. Max: 84
Size frequencies: {84: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (8, 7, 4)
1 canonical representatives with 56 codes in total
Distribution: Min: 56, Median: 56. Max: 56
Size frequencies: {56: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
0 canonical classes in R-L and not in (+)-tsum-TRS, representing 0 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 56 codes in R-L and 28 codes in (+)-tsum-TRS.

(q, n, k): (8, 8, 3)
1 canonical representatives with 36 codes in total
Distribution: Min: 36, Median: 36. Max: 36
Size frequencies: {36: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (8, 8, 5)
1 canonical representatives with 28 codes in total
Distribution: Min: 28, Median: 28. Max: 28
Size frequencies: {28: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
0 canonical classes in R-L and not in (+)-tsum-TRS, representing 0 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 28 codes in R-L and 7 codes in (+)-tsum-TRS.

(q, n, k): (9, 6, 3)
4 canonical representatives with 576 codes in total
Distribution: Min: 72, Median: 144. Max: 288
Size frequencies: {72: 2, 144: 1, 288: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
3 canonical classes in R-L and not in (+)-tsum-TRS, representing 288 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 288 codes in R-L and 168 codes in (+)-tsum-TRS.

(q, n, k): (9, 7, 3)
2 canonical representatives with 144 codes in total
Distribution: Min: 72, Median: 72. Max: 72
Size frequencies: {72: 2}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
2 canonical classes in R-L and not in (+)-tsum-TRS, representing 144 codes.
1 canonical classes in (+)-tsum-TRS and not in R-L, representing 24 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-tsum-TRS.

(q, n, k): (9, 7, 4)
3 canonical representatives with 168 codes in total
Distribution: Min: 24, Median: 72. Max: 72
Size frequencies: {24: 1, 72: 2}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (9, 8, 4)
1 canonical representatives with 24 codes in total
Distribution: Min: 24, Median: 24. Max: 24
Size frequencies: {24: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (11, 6, 3)
10 canonical representatives with 2992 codes in total
Distribution: Min: 22, Median: 220. Max: 880
Size frequencies: {880: 1, 330: 3, 220: 5, 22: 1}
9 canonical classes in R-L and not in (*)-twisted-TRS, representing 2662 codes.
0 canonical classes in (*)-twisted-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 330 codes in R-L and 5 codes in (*)-twisted-TRS.
There were no (+)-twisted-TRS of these parameters
0 canonical classes in R-L and not in (+)-tsum-TRS, representing 0 codes.
2 canonical classes in (+)-tsum-TRS and not in R-L, representing 70 codes.
10 canonical classes in both maps, representing 2992 codes in R-L and 1120 codes in (+)-tsum-TRS.

(q, n, k): (11, 7, 3)
10 canonical representatives with 1584 codes in total
Distribution: Min: 44, Median: 220. Max: 220
Size frequencies: {220: 5, 44: 1, 110: 4}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
8 canonical classes in R-L and not in (+)-tsum-TRS, representing 1430 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
2 canonical classes in both maps, representing 154 codes in R-L and 70 codes in (+)-tsum-TRS.

(q, n, k): (11, 7, 4)
10 canonical representatives with 1309 codes in total
Distribution: Min: 44, Median: 110. Max: 220
Size frequencies: {220: 2, 44: 1, 165: 1, 110: 6}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
6 canonical classes in R-L and not in (+)-tsum-TRS, representing 935 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
4 canonical classes in both maps, representing 374 codes in R-L and 320 codes in (+)-tsum-TRS.

(q, n, k): (11, 8, 3)
4 canonical representatives with 352 codes in total
Distribution: Min: 22, Median: 110. Max: 110
Size frequencies: {110: 3, 22: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (11, 8, 4)
2 canonical representatives with 77 codes in total
Distribution: Min: 22, Median: 55. Max: 55
Size frequencies: {22: 1, 55: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (11, 8, 5)
4 canonical representatives with 352 codes in total
Distribution: Min: 22, Median: 110. Max: 110
Size frequencies: {110: 3, 22: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (13, 6, 3)
19 canonical representatives with 10816 codes in total
Distribution: Min: 52, Median: 468. Max: 1248
Size frequencies: {1248: 3, 936: 2, 624: 3, 52: 1, 312: 3, 468: 4, 156: 3}
15 canonical classes in R-L and not in (*)-twisted-TRS, representing 8008 codes.
1 canonical classes in (*)-twisted-TRS and not in R-L, representing 2 codes.
4 canonical classes in both maps, representing 2808 codes in R-L and 40 codes in (*)-twisted-TRS.
There were no (+)-twisted-TRS of these parameters
2 canonical classes in R-L and not in (+)-tsum-TRS, representing 208 codes.
2 canonical classes in (+)-tsum-TRS and not in R-L, representing 432 codes.
17 canonical classes in both maps, representing 10608 codes in R-L and 6044 codes in (+)-tsum-TRS.

(q, n, k): (13, 7, 3)
35 canonical representatives with 8944 codes in total
Distribution: Min: 52, Median: 312. Max: 312
Size frequencies: {312: 23, 156: 11, 52: 1}
34 canonical classes in R-L and not in (*)-twisted-TRS, representing 8892 codes.
1 canonical classes in (*)-twisted-TRS and not in R-L, representing 4 codes.
1 canonical classes in both maps, representing 52 codes in R-L and 2 codes in (*)-twisted-TRS.
There were no (+)-twisted-TRS of these parameters
33 canonical classes in R-L and not in (+)-tsum-TRS, representing 8476 codes.
6 canonical classes in (+)-tsum-TRS and not in R-L, representing 1080 codes.
2 canonical classes in both maps, representing 468 codes in R-L and 216 codes in (+)-tsum-TRS.

(q, n, k): (13, 8, 3)
16 canonical representatives with 4160 codes in total
Distribution: Min: 104, Median: 312. Max: 312
Size frequencies: {104: 1, 312: 11, 156: 4}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (13, 8, 4)
10 canonical representatives with 1404 codes in total
Distribution: Min: 78, Median: 156. Max: 156
Size frequencies: {156: 8, 78: 2}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (13, 8, 5)
16 canonical representatives with 3328 codes in total
Distribution: Min: 52, Median: 156. Max: 312
Size frequencies: {312: 6, 156: 9, 52: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
10 canonical classes in R-L and not in (+)-tsum-TRS, representing 2496 codes.
0 canonical classes in (+)-tsum-TRS and not in R-L, representing 0 codes.
6 canonical classes in both maps, representing 832 codes in R-L and 768 codes in (+)-tsum-TRS.

(q, n, k): (13, 9, 3)
5 canonical representatives with 832 codes in total
Distribution: Min: 52, Median: 156. Max: 312
Size frequencies: {312: 1, 156: 3, 52: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (13, 9, 6)
5 canonical representatives with 832 codes in total
Distribution: Min: 52, Median: 156. Max: 312
Size frequencies: {312: 1, 156: 3, 52: 1}
There were no (*)-twisted-TRS of these parameters
There were no (+)-twisted-TRS of these parameters
There were no (+)-tsum-TRS of these parameters

(q, n, k): (16, 6, 3)
53 canonical representatives with 49868 codes in total
Distribution: Min: 60, Median: 480. Max: 5280
Size frequencies: {1440: 23, 480: 10, 68: 1, 360: 10, 5280: 1, 1320: 2, 60: 6}
52 canonical classes in R-L and not in (*)-twisted-TRS, representing 48428 codes.
0 canonical classes in (*)-twisted-TRS and not in R-L, representing 0 codes.
1 canonical classes in both maps, representing 1440 codes in R-L and 10 codes in (*)-twisted-TRS.
16 canonical classes in R-L and not in (+)-twisted-TRS, representing 10508 codes.
0 canonical classes in (+)-twisted-TRS and not in R-L, representing 0 codes.
37 canonical classes in both maps, representing 39360 codes in R-L and 10080 codes in (+)-twisted-TRS.
6 canonical classes in R-L and not in (+)-tsum-TRS, representing 360 codes.
8 canonical classes in (+)-tsum-TRS and not in R-L, representing 2520 codes.
47 canonical classes in both maps, representing 49508 codes in R-L and 48195 codes in (+)-tsum-TRS.


(q, n, k): (16, 7, 3)
260 canonical representatives with 66136 codes in total
Distribution: Min: 120, Median: 240. Max: 2040
Size frequencies: {120: 56, 240: 196, 680: 2, 2040: 5, 816: 1}
There were no (*)-twisted-TRS of these parameters
260 canonical classes in R-L and not in (+)-twisted-TRS, representing 66136 codes.
36 canonical classes in (+)-twisted-TRS and not in R-L, representing 4320 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
248 canonical classes in R-L and not in (+)-tsum-TRS, representing 62680 codes.
104 canonical classes in (+)-tsum-TRS and not in R-L, representing 27000 codes.
12 canonical classes in both maps, representing 3456 codes in R-L and 2520 codes in (+)-tsum-TRS.

(q, n, k): (16, 7, 4)
194 canonical representatives with 54096 codes in total
Distribution: Min: 240, Median: 240. Max: 720
Size frequencies: {240: 167, 336: 1, 720: 5, 480: 21}
There were no (*)-twisted-TRS of these parameters
194 canonical classes in R-L and not in (+)-twisted-TRS, representing 54096 codes.
64 canonical classes in (+)-twisted-TRS and not in R-L, representing 4320 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
180 canonical classes in R-L and not in (+)-tsum-TRS, representing 48240 codes.
170 canonical classes in (+)-tsum-TRS and not in R-L, representing 34080 codes.
14 canonical classes in both maps, representing 5856 codes in R-L and 9000 codes in (+)-tsum-TRS.

(q, n, k): (16, 8, 3)
214 canonical representatives with 61688 codes in total
Distribution: Min: 120, Median: 240. Max: 4080
Size frequencies: {1360: 2, 4080: 1, 240: 148, 408: 1, 2040: 6, 120: 56}
There were no (*)-twisted-TRS of these parameters
214 canonical classes in R-L and not in (+)-twisted-TRS, representing 61688 codes.
9 canonical classes in (+)-twisted-TRS and not in R-L, representing 1080 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
214 canonical classes in R-L and not in (+)-tsum-TRS, representing 61688 codes.
45 canonical classes in (+)-tsum-TRS and not in R-L, representing 10125 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-tsum-TRS.

(q, n, k): (16, 8, 4)
132 canonical representatives with 31488 codes in total
Distribution: Min: 48, Median: 240. Max: 240
Size frequencies: {48: 1, 240: 131}
There were no (*)-twisted-TRS of these parameters
132 canonical classes in R-L and not in (+)-twisted-TRS, representing 31488 codes.
16 canonical classes in (+)-twisted-TRS and not in R-L, representing 1080 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
132 canonical classes in R-L and not in (+)-tsum-TRS, representing 31488 codes.
51 canonical classes in (+)-tsum-TRS and not in R-L, representing 9720 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-tsum-TRS.

(q, n, k): (16, 8, 5)
186 canonical representatives with 43528 codes in total
Distribution: Min: 120, Median: 240. Max: 1680
Size frequencies: {240: 120, 168: 1, 1680: 1, 840: 6, 560: 2, 120: 56}
There were no (*)-twisted-TRS of these parameters
186 canonical classes in R-L and not in (+)-twisted-TRS, representing 43528 codes.
9 canonical classes in (+)-twisted-TRS and not in R-L, representing 1080 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
176 canonical classes in R-L and not in (+)-tsum-TRS, representing 35520 codes.
73 canonical classes in (+)-tsum-TRS and not in R-L, representing 16425 codes.
10 canonical classes in both maps, representing 8008 codes in R-L and 10725 codes in (+)-tsum-TRS.

(q, n, k): (16, 9, 3)
105 canonical representatives with 43510 codes in total
Distribution: Min: 30, Median: 240. Max: 4080
Size frequencies: {4080: 3, 680: 2, 240: 72, 2040: 5, 30: 8, 120: 14, 510: 1}
There were no (*)-twisted-TRS of these parameters
105 canonical classes in R-L and not in (+)-twisted-TRS, representing 43510 codes.
1 canonical classes in (+)-twisted-TRS and not in R-L, representing 120 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
105 canonical classes in R-L and not in (+)-tsum-TRS, representing 43510 codes.
10 canonical classes in (+)-tsum-TRS and not in R-L, representing 2250 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-tsum-TRS.

(q, n, k): (16, 9, 4)
45 canonical representatives with 10800 codes in total
Distribution: Min: 240, Median: 240. Max: 240
Size frequencies: {240: 45}
There were no (*)-twisted-TRS of these parameters
45 canonical classes in R-L and not in (+)-twisted-TRS, representing 10800 codes.
8 canonical classes in (+)-twisted-TRS and not in R-L, representing 120 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-twisted-TRS.
45 canonical classes in R-L and not in (+)-tsum-TRS, representing 10800 codes.
15 canonical classes in (+)-tsum-TRS and not in R-L, representing 1080 codes.
0 canonical classes in both maps, representing 0 codes in R-L and 0 codes in (+)-tsum-TRS.

